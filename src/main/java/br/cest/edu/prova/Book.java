package br.cest.edu.prova;

public class Book extends Item{
	private String author= "Gerald Klingbeil";
	private String edition = "1ª edição";
	private String volume = "impresso";
	
	
	public Book(String author) {
		super(author);
	}
	
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	
	public String getEdition() {
		return edition;
	}
	public void setEdition(String edition) {
		this.edition = edition;
	}
	
	public String getVolume() {
		return volume;
	}
	public void setVolume(String volume) {
		this.volume = volume;
	}
	
	
}
