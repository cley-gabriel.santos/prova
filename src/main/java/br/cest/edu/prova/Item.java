package br.cest.edu.prova;

public class Item {
	private String title;
	private String publisher = "CPB";
	private int yearpublisher = 2021;
	private String isnb = "978-65-86391-33-6"; 
	private double prive = 48.90 ; 
	
	
	public Item(String title) {
		this.title = title;
	}
	//
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	
	public int getYearpublisher() {
		return yearpublisher;
	}
	public void setYearpublisher(int yearpublisher) {
		this.yearpublisher = yearpublisher;
	}
	
	public String getIsnb() {
		return isnb;
	}
	public void setIsnb(String isnb) {
		this.isnb = isnb;
	}
	
	public double getPrive() {
		return prive;
	}
	public void setPrive(double prive) {
		this.prive = prive;
	}
	
	

}
