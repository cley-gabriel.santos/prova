package br.cest.edu.prova;

import java.util.Scanner;

public class ParImpar {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		  
		  int n;
		  System.out.print("Informe um numero: ");
		  n = sc.nextInt();
		  
		  if(n%2 == 0){
		   System.out.println("O número informado é Par " + "[" +n+ "]");
		  }
		  
		  else {
		   System.out.println("O número informado é Impar " + "[" +n+ "]");
		  }
	}

}
